package teste;

public class Conta {
		private int id; 
		private String user;
		private String senha;
		private float capital;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getSenha() {
			return senha;
		}
		public void setSenha(String senha) {
			this.senha = senha;
		}
		public float getCapital() {
			return capital;
		}
		public void setCapital(float capital) {
			this.capital = capital;
		}
}