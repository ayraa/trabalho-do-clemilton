package teste;

public class Credito {
	private int id;
	private int idconta;
	private float limite;
	private String melhordia;
	private String vencimento;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdconta() {
		return idconta;
	}
	public void setIdconta(int idconta) {
		this.idconta = idconta;
	}
	public float getLimite() {
		return limite;
	}
	public void setLimite(float limite) {
		this.limite = limite;
	}
	public String getMelhordia() {
		return melhordia;
	}
	public void setMelhordia(String melhordia) {
		this.melhordia = melhordia;
	}
	public String getVencimento() {
		return vencimento;
	}
	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}
	

}
