<?php
	class Conta{
		private $user;
		private $senha;
		private $id;
		private $capital;
		public function getCapital(){
			return $this->capital;
		}
		public function setCapital($c){
			$this->capital= (isset($c)) ? $c : NULL;
		}
		public function getUser(){
			return $this->user;
		}
		public function setUser($u){
			$this->user= (isset($u)) ? $u : NULL;
		}
		public function getSenha(){
			return $this->senha;
		}
		public function setSenha($s){
			$this->senha= (isset($s)) ? $s : NULL;
		}
		public function getId(){
			return $this->id;
		}
		public function setId($i){
			$this->id= (isset($i)) ? $i : NULL;
		}
	}
?>