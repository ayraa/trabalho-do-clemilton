<?php
class Credito{
	private $id;
	private $idconta;
	private $limite;
	private $melhordia;
	private $vencimento;
	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id= (isset($i)) ? $i : NULL;
	}
	public function getIdConta(){
		return $this->idconta;
	}
	public function setIdConta($ic){
		$this->idconta= (isset($ic)) ? $ic : NULL;
	}
	public function getLimite(){
		return $this->limite;
	}
	public function setLimite($l){
		$this->limite= (isset($l)) ? $l : NULL;
	}
	public function getMelhordia(){
		return $this->melhordia;
	}
	public function setMelhordia($im){
		$this->melhordia= (isset($m)) ? $m : NULL;
	}
	public function getVencimento(){
		return $this->vencimento;
	}
	public function setVencimento($v){
		$this->vencimento= (isset($v)) ? $v : NULL;
	}
}
?>