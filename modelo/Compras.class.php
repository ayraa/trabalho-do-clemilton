<?php
class Compras{
	private $produto;
	private $valor;
	private $dataCompra;
	private $idconta;
	private $idcredito;
	private $iddebito;
	private $id;
	public function getProduto(){
		return $this->produto;
	}
	public function setProduto($p){
		$this->produto= (isset($p)) ? $p : NULL;
	}
	public function getValor(){
		return $this->valor;
	}
	public function setValor($v){
		$this->valor= (isset($v)) ? $v : NULL;
	}
	public function getDataCompra(){
		return $this->dataCompra;
	}
	public function setDataCompra($d){
		$this->dataCompra= (isset($d)) ? $d : NULL;
	}
	public function getIdConta(){
		return $this->idconta;
	}
	public function setIdConta($ic){
		$this->idconta= (isset($ic)) ? $ic : NULL;
	}
	public function getIdCredito(){
		return $this->idcredito;
	}
	public function setIdCredito($icr){
		$this->idcredito= (isset($icr)) ? $icr : NULL;
	}
	public function getIdDebito(){
		return $this->iddebito;
	}
	public function setIdDebito($id){
		$this->idbebito= (isset($id)) ? $id : NULL;
	}
	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id= (isset($i)) ? $i : NULL;
	}



}
?>