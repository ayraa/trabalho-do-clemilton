<?php
	class Cartao{
		private $numero;
		private $senha;
		private $tipo;
		private $vencimento;
		private $limite;
		private $id;
		public function getNumero(){
			return $this->numero;
		}
		public function setNumero($n){
			$this->numero= (isset($n)) ? $n : NULL;
		}
		public function getSenha(){
			return $this->senha;
		}
		public function setSenha($s){
			$this->senha= (isset($s)) ? $s : NULL;
		}
		public function getTipo(){
			return $this->tipo;
		}
		public function setTipo($t){
			$this->tipo= (isset($t)) ? $t : NULL;
		}
		public function getVencimento(){
			return $this->vencimento;
		}
		public function setVencimento($t){
			$this->vencimento= (isset($t)) ? $t : NULL;
		}
		public function getLimite(){
			return $this->limite;
		}
		public function setLimite($l){
			$this->limite= (isset($l)) ? $l : NULL;
		}
		public function getId(){
			return $this->id;
		}
		public function setId($i){
			$this->id= (isset($i)) ? $i : NULL;
		}
	}
?>