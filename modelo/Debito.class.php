<?php
class Debito{
	private $id;
	private $idconta;
	private $valor;
	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id= (isset($i)) ? $i : NULL;
	}
	public function getIdConta(){
		return $this->idconta;
	}
	public function setIdConta($ic){
		$this->idconta= (isset($ic)) ? $ic : NULL;
	}
	public function getValor(){
		return $this->valor;
	}
	public function setValor($v){
		$this->valor= (isset($v)) ? $v : NULL;
	} 
}
?>