package teste;

public class Compras {
	private String produto;
	private int valor;
	private int dataCompra;
	private int idconta;
	private int idcredito;
	private int iddebito;
	private int id;
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(int dataCompra) {
		this.dataCompra = dataCompra;
	}
	public int getIdconta() {
		return idconta;
	}
	public void setIdconta(int idconta) {
		this.idconta = idconta;
	}
	public int getIdcredito() {
		return idcredito;
	}
	public void setIdcredito(int idcredito) {
		this.idcredito = idcredito;
	}
	public int getIddebito() {
		return iddebito;
	}
	public void setIddebito(int iddebito) {
		this.iddebito = iddebito;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}